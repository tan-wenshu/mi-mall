import Vuex from 'vuex'
import Vue from 'vue'
import mutations from './mutations'
import actions from './action'

Vue.use(Vuex)

const state = {
  username:'',
  cartCount:0
}
export default new Vuex.Store({
  state,
  mutations,
  actions
})

let baseURL;

switch (process.env.NODE_ENV) {
    case 'development':
        baseURL = 'http://dev-mall-pre.springboot.con/api';
        break;

    case 'test':
        baseURL = 'http://test-mall-pre.springboot.con/api';
        break;

    case 'production':
        baseURL = 'http://mall-pre.springboot.con/api';
        break;

    default:
        baseURL = 'http://mall-pre.springboot.con/api';
        break;
}

export default {
    baseURL
}